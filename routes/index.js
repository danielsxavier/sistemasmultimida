var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { videos: ['akiyo_cif', 
                                'bridge-close_cif', 
                                'bridge-far_cif', 
                                'bus_cif', 
                                'coastguard_cif', 
                                'container_cif', 
                                'flower_cif',
                                'football_cif',
                                'foreman_cif',
                                'highway_cif'] });
});

module.exports = router;
